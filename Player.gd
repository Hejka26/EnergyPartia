extends KinematicBody
var gravity : Vector3 = Vector3.DOWN * 12.0
var speed : float = 4.0
var velocity : Vector3 = Vector3()
var direction_x = "none"
var direction_y = "none"
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	var sink = velocity.y # we want to stor eonly the downward velocity for this game ( you would not normaly do this
	# This removes any latteral movements we dont want 
	velocity = Vector3( 0.0, sink , 0.0 )
	if is_on_floor():
		velocity += gravity * 0.1 * delta
	else:
		velocity += gravity * delta
	if (Input.is_action_just_pressed("move_left")):
		direction_x = "left"
	if (Input.is_action_just_pressed("move_right")):
		direction_x = "right"
	if not (Input.is_action_pressed("move_left")) and not (Input.is_action_pressed("move_right")):
		direction_x = "none"
	if (Input.is_action_just_pressed("move_up")):
		direction_y = "up"
	if (Input.is_action_just_pressed("move_down")):
		direction_y = "down"
	if not (Input.is_action_pressed("move_up")) and not (Input.is_action_pressed("move_down")):
		direction_y = "none"
	
	if direction_x == "left":
				velocity += Vector3.LEFT * speed
				self.set_rotation( Vector3( 0 , -.5*PI , 0 ) )
	if direction_x == "right":
				velocity += Vector3.RIGHT * speed
				self.set_rotation( Vector3( 0 , .5*PI , 0 ))
	if direction_y == "up":
				velocity += Vector3.FORWARD * speed
				self.set_rotation( Vector3( 0 , PI , 0 ) )
	if direction_y == "down":
				velocity += Vector3.BACK * speed
				self.set_rotation( Vector3( 0 ,2*PI , 0 ) )
		

	velocity = move_and_slide( velocity , Vector3.UP)

