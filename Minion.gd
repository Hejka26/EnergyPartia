extends KinematicBody
var gravity : Vector3 = Vector3.DOWN * 12.0
var speed : float = 4.0
var velocity : Vector3 = Vector3()
var direction_x = "none"
var direction_y = "none"
#onready var minion = get_node(get_path())
#onready var target = get_node("root/Player3d")
onready var label = get_node("../Camera/RichTextLabel")
onready var labelel = get_node("../Camera/RichTextLabel2")
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	var minion_pos = get_node(get_path()).get_global_transform()
	var target_pos = get_node("../Player3d/Target").get_global_transform()
	var sink = velocity.y # we want to stor eonly the downward velocity for this game ( you would not normaly do this
	# This removes any latteral movements we dont want 
	velocity = Vector3( 0.0, sink , 0.0 )
	if is_on_floor():
		velocity += gravity * 0.1 * delta
	else:
		velocity += gravity * delta
		
		
	if target_pos[1] > minion_pos[1]:
		direction_x = "left"
	if target_pos[1] < minion_pos[1]:
		direction_x = "right"
	if target_pos[1] == minion_pos[1]:
		direction_x = "none"	
	
	if target_pos[3] < minion_pos[3]:
		direction_y = "up"
	if target_pos[3] > minion_pos[3]:
		direction_y = "down"
	if target_pos[3] == minion_pos[3]:
		direction_y = "none"
		

	
	if direction_x == "left":
				velocity += Vector3.LEFT * speed
				self.set_rotation( Vector3( 0 , -.5*PI , 0 ) )
	if direction_x == "right":
				velocity += Vector3.RIGHT * speed
				self.set_rotation( Vector3( 0 , .5*PI , 0 ))
	if direction_y == "up":
				velocity += Vector3.FORWARD * speed
				self.set_rotation( Vector3( 0 , PI , 0 ) )
	if direction_y == "down":
				velocity += Vector3.BACK * speed
				self.set_rotation( Vector3( 0 ,2*PI , 0 ) )
		

	velocity = move_and_slide( velocity , Vector3.UP)

	var STR_target_pos= str(target_pos)
	label.set_text(STR_target_pos)
	var STR_minion_pos= str(minion_pos)
	labelel.set_text(STR_minion_pos)
